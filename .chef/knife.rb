cookbook_path    ["cookbooks"]
node_path        "nodes"
role_path        "roles"
environment_path "environments"
data_bag_path    "data_bags"
#encrypted_data_bag_secret "data_bag_key"

knife[:berkshelf_path] = ["cookbooks", "~/.berkshelf/cookbooks"]
chef_zero.enabled  true
local_mode true
node_name 'hardy'
client_key               '~/.ssh/id_rsa'
chef_server_url          'localhost:8889'