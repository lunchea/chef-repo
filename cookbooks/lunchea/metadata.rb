name             'lunchea'
maintainer       'The Authors'
maintainer_email 'you@example.com'
license          'all_rights'
description      'Installs/Configures lunchea'
long_description 'Installs/Configures lunchea'
version          '0.1.0'

depends "firewall"
depends "fail2ban"
depends "openssh"
depends "users"
depends "chef-solo-search"
depends "deploy_key"
depends "ssh"
depends "sudo"
depends "nginx"
depends "rbenv"
depends "postgresql"
depends "database"
depends "redisio"
