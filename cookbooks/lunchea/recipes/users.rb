include_recipe "users"
include_recipe "sudo"

users_manage "rbenv" do
  action :create
end

deploy_key "gitlab" do
  provider Chef::Provider::DeployKeyGitlab
  credentials({
    :token => node["deployment_auth"]["gitlab"]
  })
  api_url "https://gitlab.com"
  repo 260602
  path "/home/scixiv/.ssh"
  owner "scixiv"
  group "scixiv"
  action :add
end


deploy_key "gitlab" do
  provider Chef::Provider::DeployKeyGitlab
  credentials({
    :token => node["deployment_auth"]["gitlab"]
  })
  api_url "https://gitlab.com"
  repo 260612
  path "/home/scixiv/.ssh"
  owner "scixiv"
  group "scixiv"
  action :add
end


deploy_key "gitlab" do
  provider Chef::Provider::DeployKeyGitlab
  credentials({
    :token => node["deployment_auth"]["gitlab"]
  })
  api_url "https://gitlab.com"
  repo 260600
  path "/home/lunchea/.ssh"
  owner "lunchea"
  group "lunchea"
  action :add
end

deploy_key "gitlab" do
  provider Chef::Provider::DeployKeyGitlab
  credentials({
    :token => node["deployment_auth"]["gitlab"]
  })
  api_url "https://gitlab.com"
  repo 260589
  path "/home/lunchea/.ssh"
  owner "lunchea"
  group "lunchea"
  action :add
end

deploy_key "github" do
  provider Chef::Provider::DeployKeyGithub
  credentials({
    :token => node["deployment_auth"]["github"]
  })
  repo "hardywu/wuki"
  path "/home/wuinm/.ssh"
  owner "wuinm"
  group "wuinm"
  action :add
end

ssh_config "gitlab.com" do
  options "IdentityFile" => "/home/scixiv/.ssh/gitlab"
  user "scixiv"
end

ssh_config "gitlab.com" do
  options "IdentityFile" => "/home/lunchea/.ssh/gitlab"
  user "lunchea"
end

ssh_config "github.com" do
  options "IdentityFile" => "/home/wuinm/.ssh/github"
  user "wuinm"
end
