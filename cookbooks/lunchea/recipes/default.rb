#
# Cookbook Name:: lunchea
# Recipe:: default
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe "firewall::default"
include_recipe "fail2ban::default"
include_recipe "openssh::default"

firewall_rule 'http/https' do
  protocol :tcp
  port     [80, 443]
  action   :allow
end


include_recipe "rbenv::default"
include_recipe "rbenv::ruby_build"

rbenv_ruby "2.1.2"
rbenv_gem "bundler" do
  ruby_version "2.1.2"
end

include_recipe "postgresql::server"
include_recipe "database::postgresql"
include_recipe "postgresql::ruby"

# postgresql_connection_info = {
#   host: '/var/run/postgresql',
#   user: 'postgres',
#   dbname: 'postgres'
# }
postgresql_connection_info = {
  :host     => '127.0.0.1',
  :port     => node['postgresql']['config']['port'],
  :username => 'postgres',
  :password => node['postgresql']['password']['postgres']
}
postgresql_database_user 'scixiv' do
  connection postgresql_connection_info
  action :create
end
postgresql_database_user 'lunchea' do
  connection postgresql_connection_info
  action :create
end
postgresql_database_user 'wuinm' do
  connection postgresql_connection_info
  action :create
end
postgresql_database 'lunchea_production' do
  connection postgresql_connection_info
  owner 'lunchea'
  action :create
end
postgresql_database 'scixiv_production' do
  connection postgresql_connection_info
  owner 'scixiv'
  action :create
end
postgresql_database 'wuinm_production' do
  connection postgresql_connection_info
  owner 'wuinm'
  action :create
end

include_recipe "redisio::default"
include_recipe "redisio::enable"
include_recipe 'nginx::default'
